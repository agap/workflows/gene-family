#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Gaetan Droc
import os
import sys
import argparse
from Bio import SeqIO

 
parser = argparse.ArgumentParser(
    description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("-i","--input", help="fasta file")
parser.add_argument("-o","--output", help="output file")
parser.add_argument("-s","--species", help="species name")

args = parser.parse_args()



with open(args.input) as original, open(args.output, 'w') as corrected:
    records = SeqIO.parse(original, 'fasta')
    for record in records:
        record.id = record.id + "_" + args.species
        record.description = ""
        SeqIO.write(record, corrected, 'fasta')
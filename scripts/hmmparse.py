#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Gaetan Droc
import os
import sys
import argparse
from Bio import SearchIO



parser = argparse.ArgumentParser(
    description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("-i","--input", help="hmmer file")
parser.add_argument("-o","--output", help="output file")

args = parser.parse_args()

list_id = [] 
results = SearchIO.parse(args.input, 'hmmer3-text')
for protein in results: 
    hits = protein.hits
    if len(hits) > 0:
        for i in range(0,len(hits)): 
            hit_id = hits[i].id 
            list_id.append(hit_id)  
    with open(args.output, 'w') as fp:
        fp.write('\n'.join(list_id)) 

    
      
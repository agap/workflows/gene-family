# Snakemake workflow: Phylogenetic analysis

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 

**Table of Contents** 

  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)

## Objective

A Snakemake workflow for creating or using HMM profiles to scan proteomes and perform phylogenetic analysis.

## Dependencies

* hmmbuild : Build a profile HMM from an alignment (http://www.csb.yale.edu/userguides/seq/hmmer/docs/node19.html)
* hmmsearch : Search a sequence database with a profile HMM (http://www.csb.yale.edu/userguides/seq/hmmer/docs/node26.html)
* MAFFT : Multiple alignment program for amino acid or nucleotide sequences (https://mafft.cbrc.jp/alignment/software/)
* fasta-fetch : Fetch sequences from a FASTA sequence file. Requires an index file made by fasta-make-index. (https://meme-suite.org/meme/doc/fasta-fetch.html)
* Trimal : Automated removal of spurious sequences or poorly aligned regions from a multiple sequence alignment (http://trimal.cgenomics.org/)
* PhyML : Phylogeny software based on the maximum-likelihood principle (http://www.atgc-montpellier.fr/phyml/usersguide.php)
* RapGreen : Reconcile phylogenetic trees with the corresponding species tree. (http://southgreenplatform.github.io/rap-green/)

## Prerequisites

- Globally installed SLURM 18.08.7.1+
- Globally installed singularity 3.4.1+
- Installed Snakemake 6.0.0+

## Overview of programmed workflow
<details>
  <p align="center">
  <img src="images/rulegraph.svg">
  </p>
</details>

## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  ```
  git clone https://gitlab.cirad.fr/agap/cluster/snakemake/gene-family.git
  ```


- Change directories into the cloned repository:
  ```
  cd gene-family
  ```

- Edit the configuration file config.yaml

```
# Perform phylogenetic analysis of one or more sets of predicted peptide sequences
input_proteome:
  CITME: "/storage/replicated/cirad/web/HUBs/citrus/citrus_medica/fasta/CITME_prot.faa"
  CITMI: "/storage/replicated/cirad/web/HUBs/citrus/citrus_micrantha/fasta/CITMI_prot.faa"
  CITRE: "/storage/replicated/cirad/web/HUBs/citrus/citrus_reticulata/fasta/CITRE_prot.faa"
  CITMA: "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima/fasta/CITMA_prot.faa"
  CITCL: "/storage/replicated/cirad/web/HUBs/citrus/citrus_clementina/fasta/Cclementina_182_protein.fa"
  PONTR: "/storage/replicated/cirad/web/HUBs/citrus/poncirus_trifoliata/fasta/Ptrifoliata_565_v1.3.1.protein_primaryTranscriptOnly.fa"
  FORHI: "/storage/replicated/cirad/web/HUBs/citrus/fortunella_hindsii/fasta/fortunella_hindsii.prot.faa"
  ARATH: "/storage/replicated/cirad/web/HUBs/grass/arabidopsis_thaliana/fasta/Athaliana_447_Araport11.protein_primaryTranscriptOnly.fa"
  
# If you have already a profile HMM
use_hmm: True

# creates a profile hidden Markov model from a group of aligned sequences using by hmmerbuild
family: 
  LAX1: "data/LAX1.txt"
  NCED: "data/NCED_seed13.txt"

# use directly a HMM profile (need to set the param use_hmm = True)
hmm:
  KBox: "data/PF01486.hmm"  


# Species tree used by RapGreen
species_tree: "data/species.nhx"
# Params for hmmsearch
evalue: 1e-20

threads: 8

# -----------------------------------------
# Param for mafft
# Maximum number of iterative refinement
maxiterate: 1000
# Offset (works like gap extension penalty)
ep: 0.123
# -----------------------------------------


# -----------------------------------------
# Param for phyml
# Tree topology search operation
# Available option
# - BEST : Best of NNI and SPR
# - NNI  : Nearest Neighbor Interchange
# - SPR  : Subtree Pruning and Regraphing
tree_topology: "BEST"
# Evolution model
# - LG 
# - WAG>
# - JT
# - MtREV
# - Dayhoff
# - DCMut
# - RtREV
# - CpREV
# - VT
# - Blosum62
# - MtMam
# - MtArt
# - HIVw
# - HIVb
evolution_model: "LG"

# Bootstrap
#	int >  0: int is the number of bootstrap replicates.
#	int =  0: neither approximate likelihood ratio test nor bootstrap values are computed.
# int = -1: approximate likelihood ratio test returning aLRT statistics.
# int = -2: approximate likelihood ratio test returning Chi2-based parametric branch supports.
# int = -4: (default) SH-like branch supports alone.
# int = -5: approximate Bayes branch supports.
bootstrap: -4
# -----------------------------------------


# Module load
modules: 
  hmmer: "hmmer/3.3.2-bin"
  mafft: "mafft/7.475"
  trimal: "trimal/1.4"
  meme: "meme/5.3.3"
  perllib: "perllib/5.16.3"
  phyml: "phyml/3.1"
  java: "jre/jre.8_x64"


container:
  hmmer: "https://depot.galaxyproject.org/singularity/hmmer%3A3.4--hdbdd923_1"
  mafft: "https://depot.galaxyproject.org/singularity/mafft%3A7.525--h031d066_1"
  trimal: "https://depot.galaxyproject.org/singularity/trimal%3A1.4.1--h9f5acd7_7"
  meme: "https://depot.galaxyproject.org/singularity/meme%3A5.5.5--pl5321hda358d9_0"
  phyml: "https://depot.galaxyproject.org/singularity/phyml%3A3.3.20220408--h37cc20f_1"
  java: "https://depot.galaxyproject.org/singularity/java-jdk%3A8.0.112--1"
  biopython: "https://depot.galaxyproject.org/singularity/biopython%3A1.81"

```



- Edit the configuration file profile/config.yaml for SLURM integration
  * partition
  * mem_mb


## For Meso@LR

- Print shell commands to validate your modification (mode dry-run)

```bash
$ ./snakemake.sh dry
Building DAG of jobs...
Singularity image https://depot.galaxyproject.org/singularity/biopython%3A1.81 will be pulled.
Singularity image https://depot.galaxyproject.org/singularity/meme%3A5.5.5--pl5321hda358d9_0 will be pulled.
Singularity image https://depot.galaxyproject.org/singularity/mafft%3A7.525--h031d066_1 will be pulled.
Singularity image https://depot.galaxyproject.org/singularity/java-jdk%3A8.0.112--1 will be pulled.
Singularity image https://depot.galaxyproject.org/singularity/hmmer%3A3.4--hdbdd923_1 will be pulled.
Singularity image https://depot.galaxyproject.org/singularity/trimal%3A1.4.1--h9f5acd7_7 will be pulled.
Singularity image https://depot.galaxyproject.org/singularity/phyml%3A3.3.20220408--h37cc20f_1 will be pulled.
Job stats:
job              count    min threads    max threads
-------------  -------  -------------  -------------
add_species          7              1              1
all                  1              1              1
fasta_index          7              1              1
fasta_symlink        7              1              1
fastafetch           7              1              1
hmmparse             7              1              1
hmmsearch            7              1              1
merge_fasta          1              1              1
merge_mafft          1              1              1
phyml                1              1              1
rapgreen             1              1              1
trimal_family        1              1              1
total               48              1              1

```

- Run workflow on Meso@LR

```bash
$ sbatch snakemake.sh run
```
     
- Unlock directory

```bash
$ ./snakemake.sh unlock
```
      
- Generate DAG file

```bash
$ ./snakemake.sh dag
```
## Output 

Output directory prefixed with family_name defined in config file

* <family_name>.faa
* <family_name>.nhx
* <family_name>_trimed.aln
* <family_name>_trimed.phylip

## Example

Using KBox.hmm profile (with the option : *use_hmm: True from config.yam*)

```bashKBox/K*
-rw-r--r-- 1 X agap 113290 Feb 13 11:59 KBox/KBox.aln                               : Multiple sequence alignment of sequences matching the HMM profile, representing the protein family
-rw-rw-r-- 1 X agap  42217 Feb 13 11:58 KBox/KBox.faa                               : FASTA file with sequences from proteomes matching the HMM profile
-rw-r--r-- 1 X agap  13880 Feb 13 12:05 KBox/KBox.nhx                               
-rw-r--r-- 1 X agap  30272 Feb 13 12:05 KBox/KBox.stats
-rw-r--r-- 1 X agap   6374 Feb 13 12:05 KBox/KBox_reconcilied.nhx                   : Reconcilied tree 
-rw-r--r-- 1 X agap  28645 Feb 13 12:00 KBox/KBox_trimed.aln                        : A multiple sequence alignment of the protein family, trimmed with TrimAl for improved phylogenetic analysis
-rw-r--r-- 1 X agap  29383 Feb 13 12:00 KBox/KBox_trimed.phylip                     : Phylip output
-rw-r--r-- 1 X agap   1925 Feb 13 12:05 KBox/KBox_trimed.phylip_phyml_stats.txt     : Phyml statistics
-rw-r--r-- 1 X agap   9005 Feb 13 12:05 KBox/KBox_trimed.phylip_phyml_tree.txt      : Phylogenetic tree
```

For example, you can use iTOL or ETE Toolkit to vizualize phylogenetic trees online

* ETE Toolkit https://etetoolkit.org/treeview/ 
* iTOL https://itol.embl.de/
import os 
import sys 
import subprocess,sys,re
from os.path import join,basename 
from snakemake.io import expand, multiext, glob_wildcards, directory

configfile: "config.yaml"


input_proteome = config["input_proteome"]

is_hmm = config["use_hmm"]
if is_hmm:
    input_family = config["hmm"]
else:
    input_family = config["family"] 

wildcard_constraints:
    database = "|".join(input_proteome.keys()),
    family = "|".join(input_family.keys())    

rule all:
    input: 
        expand("{family}/{family}.nhx" , family=input_family.keys()  )


rule mafft:
    input: 
        family = lambda wildcards: input_family[wildcards.family] 
    output: 
        "{family}/profile.aln"
    envmodules:
        config["modules"]["mafft"]
    singularity:
        config["container"]["mafft"]
    params:
        maxiterate = config["maxiterate"],
        ep = config["ep"]
    threads: 
        config["threads"]
    shell:"""
        mafft --maxiterate {params.maxiterate}  --ep {params.ep} --thread {threads} {input} > {output}
    """
    
rule trimal:
    input:
        rules.mafft.output
    output: 
        "{family}/profile_trimed.aln"
    envmodules:
        config["modules"]["trimal"]
    singularity:
        config["container"]["trimal"] 
    params:
        advanced_parameter = config["advanced_parameter"]
    shell:"""
        trimal -in {input} -out {output} -fasta {params.advanced_parameter}
    """

rule hmmbuild:
    input:
        rules.trimal.output
    output:
        "{family}/{family}.hmm"
    envmodules:
        config["modules"]["hmmer"]
    singularity:
        config["container"]["hmmer"]
    threads: 
        config["threads"]
    shell:"""
        hmmbuild --cpu {threads} --informat afa {output} {input}
    """
    

rule hmmsearch:
    input:
        db = lambda wildcards: input_proteome[wildcards.database], 
        hmm = lambda wildcards: input_family[wildcards.family] if config["use_hmm"] else rules.hmmbuild.output 
    output:
        "{family}/{database}.hmm"
    envmodules:
        config["modules"]["hmmer"]
    singularity:
        config["container"]["hmmer"]
    params:
        evalue = config["evalue"]
    threads: 
        config["threads"]
    shell:"""
        hmmsearch -o {output} -E {params.evalue} --cpu {threads} {input.hmm} {input.db}
    """


rule hmmparse:
    input:
        rules.hmmsearch.output
    output:
        "{family}/{database}.tsv"
    singularity:
        config["container"]["biopython"]
    shell:"""
        scripts/hmmparse.py --input {input} --output {output}
    """

############################################
### Create a symoblic link with a nice name for the fasta files.
rule fasta_symlink:
    input:
        lambda wildcards: input_proteome[wildcards.database]
    output:
        "logs/{database}.fasta" 
    shell:"""
        ln -s {input} {output}
    """

###########################################
### Create fasta index 
rule fasta_index:
    input:
        "logs/{database}.fasta"
    output:
        "logs/{database}.fasta.index"
    envmodules:
        config["modules"]["meme"]
    singularity:
        config["container"]["meme"]
    shell:"""
        fasta-make-index {input} -f 
    """


rule fastafetch:
    input: 
        db = "logs/{database}.fasta",
        index=rules.fasta_index.output,
        list=rules.hmmparse.output
    output:
        "{family}/{database}_temp.faa"
    envmodules:
        config["modules"]["meme"]
    singularity:
        config["container"]["meme"]
    shell:"""
        fasta-fetch {input.db} -f {input.list} > {output}
    """

# Add species name for each record in fasta
rule add_species:
    input:
        rules.fastafetch.output
    output:
        "{family}/{database}.faa"
    singularity:
        config["container"]["biopython"]
    params:
        species = "{database}"
    shell:"""
        scripts/add_species.py --input {input} --output {output} --species {params.species}
    """ 

# Merge fasta 
rule merge_fasta:
    input:
        expand("{{family}}/{database}.faa", database=input_proteome.keys())
    output:
        "{family}/{family}.faa" 
    shell:"""
        cat {input} > {output}
    """

use rule mafft as merge_mafft with:
    input:
         rules.merge_fasta.output
    output:
        "{family}/{family}.aln"

        
# rule mafft_merge_fasta:
#     input:
#          rules.merge_fasta.output
#     output:
#         "{family}/{family}.aln"
#     envmodules:
#         config["modules"]["mafft"]
#     singularity:
#         config["modules"]["mafft"]
#     threads: 
#         config["threads"]
#     shell:"""
#         mafft --maxiterate  1000  --ep 0.123 --thread {threads} {input} > {output}
#     """
    

rule trimal_family:
    input:
        rules.merge_mafft.output
    output:
        aln = "{family}/{family}_trimed.aln",
        phylip = "{family}/{family}_trimed.phylip"        
    envmodules:
        config["modules"]["trimal"]
    singularity:
        config["container"]["trimal"]
    params:
        advanced_parameter = config["advanced_parameter"] 
    shell:"""
        trimal -in {input} -out {output.phylip} -phylip {params.advanced_parameter};
        trimal -in {input} -out {output.aln} {params.advanced_parameter}
    """

        
rule phyml:
    input:
        rules.trimal_family.output.phylip
    output: 
        stats = "{family}/{family}_trimed.phylip_phyml_stats.txt",
        gene_tree = "{family}/{family}_trimed.phylip_phyml_tree.txt"
    envmodules:
        config["modules"]["phyml"]
    singularity:
        config["container"]["phyml"]
    params:
        evolution_model = config["evolution_model"],
        tree_topology = config["tree_topology"],
        bootstrap = config["bootstrap"]
    shell:"""
        phyml -i {input} -d aa -m {params.evolution_model} -s {params.tree_topology} -b {params.bootstrap}
    """

rule rapgreen:
    input:
        gene_tree = rules.phyml.output.gene_tree,
        species_tree = config["species_tree"]
    output:
        nhx = "{family}/{family}.nhx",
        stats = "{family}/{family}.stats",
        reconcilied = "{family}/{family}_reconcilied.nhx"        
    envmodules:
        config["modules"]["java"]
    singularity:
        config["container"]["java"]
    params:
        gene_threshold = config["gene_threshold"],
        species_threshold = config["species_threshold"],
        polymorphism_threshold = config["polymorphism_threshold"]
    shell:"""
        java -jar scripts/RapGreen.jar -g {input.gene_tree} -s {input.species_tree} -pt {params.polymorphism_threshold} -gt {params.gene_threshold} -st {params.species_threshold} -nhx {output.nhx} -stats {output.stats} -or {output.reconcilied}
    """
